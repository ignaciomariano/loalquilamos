

<br/>
<span>Descripcion de la publicacion: </span><br/><br/>
<form>
            <div class="menu" style="width: 900px;">
			<div class="menu-nombre">Titulo</div>
			<input type="text" id="titulo" size="65" onblur="comprobarTexto(this.value,'error-titulo')" style="float: left;"/>
                        <div id="error-titulo" style="padding:3px; padding-left: 15px; float: left;"></div>
		</div>
		<div class="menu" style="width: 900px;">
			<div class="menu-nombre">Descripcion</div>
			<input type="text" size="65px" id="descripcion" onblur="comprobarTexto(this.value,'error-descripcion')" style="float: left;"/>
                        <div id="error-descripcion" style="padding:3px; padding-left: 15px; float: left;"></div>
		</div>
		<div class="menu" style="width: 900px;">
			<div class="menu-nombre">Tipo de inmueble</div>
			<div >
				<select   name="menu" id="id_tipo_inmueble">
				<option value="1">Casas</option>
				<option value="2">Departamentos</option>
				<option value="3">PH</option>
				<option value="4">Countries y barrios cerrados</option>
				<option value="5">Quintas</option>
				<option value="6">Terrenos o lotes</option>
				</select>
			</div>
		</div>
		<div class="menu" style="width: 900px;">
                    <div class="menu-nombre">Zona geografica:</div>
                        <select id="id_zona">
                            <option value="3642" selected>Capital Federal</option>
		            <option value="5055">Bs.As. G.B.A. Zona Norte</option>
		            <option value="2277">Bs.As. G.B.A. Zona Oeste</option>
		            <option value="3979">Bs.As. G.B.A. Zona Sur</option>
		            <option value="4314">Buenos Aires Costa Atlántica</option>
		            <option value="587" >Buenos Aires Interior</option>
		            <option value="2811">Catamarca</option>
		            <option value="3542">Chaco</option>
		            <option value="5785">Chubut</option>
		            <option value="1854">Córdoba</option>
		            <option value="488" >Corrientes</option>
		            <option value="4364">Entre Ríos</option>
		            <option value="5474">Formosa</option>
		            <option value="1571">Jujuy</option>
		            <option value="2700">La Pampa</option>
		            <option value="139" >La Rioja</option>
		            <option value="5265">Mendoza</option>
		            <option value="5169">Misiones</option>
		            <option value="5970">Neuquén</option>
		            <option value="3705">Río Negro</option>
		            <option value="2360">Salta</option>
		            <option value="295" >San Juan</option>
		            <option value="3903">San Luis</option>
		            <option value="6133">Santa Cruz</option>
		            <option value="4659">Santa Fe</option>
		            <option value="4096">Santiago Del Estero</option>
		            <option value="2350">Tierra Del Fuego</option>
		            <option value="2">Tucumán</option>
		            <option value="5540">Zona Exterior</option>
                    </select>
                </div>
                <div class="menu">
                    <div class="menu-nombre">Partido</div>
                        <select   id="id_partido">
		            <option value="3642"   >Capital Federal</option>
		            <option value="5055"   >Bs.As. G.B.A. Zona Norte</option>
		            <option value="2277"   >Bs.As. G.B.A. Zona Oeste</option>
		            <option value="3979"   >Bs.As. G.B.A. Zona Sur</option>
		            <option value="4314"   >Buenos Aires Costa Atlántica</option>
		            <option value="587"   >Buenos Aires Interior</option>
		            <option value="2811"   >Catamarca</option>
		            <option value="3542"   >Chaco</option>
		            <option value="5785"   >Chubut</option>
		            <option value="1854"   >Córdoba</option>
		            <option value="488"   >Corrientes</option>
		            <option value="4364"   >Entre Ríos</option>
		            <option value="5474"   >Formosa</option>
                        </select>
		</div>
                <div class="menu" style="width: 900px; ">
                    <div class="menu-nombre">Direccion: </div>
                    <input type="text" size="30" maxlength='30' id="direccion" onblur="comprobarTexto(this.value,'error-direccion')" style="float: left;"/>
                    <div id="error-direccion" style="padding:3px; padding-left: 15px; float: left;"></div>
                </div>
                <div class="menu">
        		<div class="menu-nombre" >Piso: </div>
        		<input type="text" size="3" maxlength='3' id="piso" onblur="comprobarNumero(this.value,'error-piso')"style="float: left;"/>
                        <div id="error-piso" style="padding:3px; padding-left: 15px; float: left;"></div>
        	</div>
        	<div class="menu">
        		<div class="menu-nombre">Ambientes: </div>
        		    <select id="ambientes">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected>4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">9</option>
                                <option value="11">Más de 11</option>
                            </select>
        	</div>
        	<div class="menu">
        		<div class="menu-nombre">Baños: </div>
        		    <select id="baños">
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">Más de 3</option>
                            </select>
        	</div>
        	<div class="menu">
        		<div class="menu-nombre">Dormitorios: </div>
        		    <select id="dormitorios">
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">Más de 4</option>
                            </select>
        	</div>
                <div class="menu">
        		<div class="menu-nombre">Cocheras: </div>
        		    <select id="cochera">
                                <option value="1" selected>No</option>
                                <option value="2">Sí</option>
                            </select>
        	</div>
                <div class="menu">
                    <div class="menu-nombre">Superficie total: </div>
                    <input type="text" size="3" maxlength='3' id="superficietot" onblur="comprobarNumero(this.value,'error-suptotal')"style="float: left;"/>
                    <div id="error-suptotal" style="padding:3px; padding-left: 15px; float: left;"></div>
            	</div>
                <div class="menu">
                    <div class="menu-nombre">Superficie cubierta: </div>
                    <input type="text" size="3" maxlength='3' id="superficiecub" onblur="comprobarNumero(this.value,'error-supcub')"style="float: left;"/>
                    <div id="error-supcub" style="padding:3px; padding-left: 15px; float: left;"></div>
                </div>
                <div class="menu">
                    <div class="menu-nombre">Expensas: </div>
                    <input type="text" size="30" maxlength='20' id="expensas" onblur="comprobarTexto(this.value,'error-expensas')"style="float: left;"/>
                    <div id="error-expensas" style="padding:3px; padding-left: 15px; float: left;"></div>
                </div>
                <div class="menu">
        		<div class="menu-nombre">Estado: </div>
        		    <select id="estado">
                                <option value="1">A refacccionar</option>
                                <option value="2">regular</option>
                                <option value="3" selected>Bueno</option>
                                <option value="4">Muy bueno</option>
                                <option value="5">Excelente</option>
                            </select>
        		
        	</div>
            	<div class="menu">
        		<div class="menu-nombre">Plantas: </div>
        		    <select id="plantas">
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">Más de 3</option>
                            </select>
        	</div>
            	<div class="menu">
        		<div class="menu-nombre">Lugar: </div>
        		    <select id="Lugar">
                                <option value="1">Barrio cerrado</option>
                                <option value="2">Country</option>
                                <option value="3" selected>Sobre calle</option>
                                <option value="4">Otro lugar</option>
                            </select>
        	</div>
        	<div class="menu">
        		<div class="menu-nombre">Sube una foto: </div>
        		<input type="file"/>
        	</div>
<div style="clear:both"></div>

<hr size="1px" noshade="noshade" color="#4faced"/>
<div style="margin-left: 20px;">
<div style="float:left; width: 50%;">
<span>Ambientes</span><br/><br/>
<div style="float:left; width: 50%;">
<input name="as_AMBIENTES" id="ANTECOCINA" type="checkbox" value="ANTECOCINA" > <label for="ANTECOCINA">Ante cocina</label><br/>
<input name="as_AMBIENTES" id="BALTERR" type="checkbox" value="BALTERR" > <label for="BALTERR">Balcón</label><br/>
<input name="as_AMBIENTES" id="BAUALT" type="checkbox" value="BAUALT" > <label for="BAUALT">Baulera/Altillo</label><br/>
<input name="as_AMBIENTES" id="BIBLIOT" type="checkbox" value="BIBLIOT" > <label for="BIBLIOT">Biblioteca</label><br/>
<input name="as_AMBIENTES" id="BODEGA" type="checkbox" value="BODEGA" > <label for="BODEGA">Bodega</label><br/>
<input name="as_AMBIENTES" id="COCI" type="checkbox" value="COCI" > <label for="COCI">Cocina</label><br/>
<input name="as_AMBIENTES" id="COME" type="checkbox" value="COME" > <label for="COME">Comedor</label><br/>
<input name="as_AMBIENTES" id="DEPSERV" type="checkbox" value="DEPSERV" > <label for="DEPSERV">Dependencia de servicio</label><br/>
<input name="as_AMBIENTES" id="DORMSUIT" type="checkbox" value="DORMSUIT" > <label for="DORMSUIT">Dormitorio en suite</label><br/>
<input name="as_AMBIENTES" id="ENTREPISO" type="checkbox" value="ENTREPISO" > <label for="ENTREPISO">Entrepiso</label><br/>
<input name="as_AMBIENTES" id="ESCRITORIO" type="checkbox" value="ESCRITORIO" > <label for="ESCRITORIO">Escritorio</label><br/>
</div>
<div style="float:left;">
<input name="as_AMBIENTES" id="ESTAR" type="checkbox" value="ESTAR" > <label for="ESTAR">Estar</label><br/>
<input name="as_AMBIENTES" id="GALERIA" type="checkbox" value="GALERIA" > <label for="GALERIA">Galería</label><br/>
<input name="as_AMBIENTES" id="LAVA" type="checkbox" value="LAVA" > <label for="LAVA">Lavadero</label><br/>
<input name="as_AMBIENTES" id="LIVI" type="checkbox" value="LIVI" > <label for="LIVI">Living</label><br/>
<input name="as_AMBIENTES" id="LIVCOM" type="checkbox" value="LIVCOM" > <label for="LIVCOM">Living comedor</label><br/>
<input name="as_AMBIENTES" id="PATI" type="checkbox" value="PATI" > <label for="PATI">Patio</label><br/>
<input name="as_AMBIENTES" id="PLAYROOM" type="checkbox" value="PLAYROOM" > <label for="PLAYROOM">Playroom</label><br/>
<input name="as_AMBIENTES" id="QUINCHO" type="checkbox" value="QUINCHO" > <label for="QUINCHO">Quincho</label><br/>
<input name="as_AMBIENTES" id="TERRA" type="checkbox" value="TERRA" > <label for="TERRA">Terraza</label><br/>
<input name="as_AMBIENTES" id="TOIL" type="checkbox" value="TOIL" > <label for="TOIL">Toilette</label><br/>
<input name="as_AMBIENTES" id="VEST" type="checkbox" value="VEST" > <label for="VEST">Vestidor</label><br/>
</div>
</div>

<div style="float:left; width: 50%;">
<span>Comodidades</span><br/><br/>
<div style="float:left; width: 59%;">
<input name="as_COMODIDADES" id="AIRAMB" type="checkbox" value="AIRAMB" > <label for="AIRAMB">Aire acondicionado por ambiente</label><br/>
<input name="as_COMODIDADES" id="ALAR" type="checkbox" value="ALAR" > <label for="ALAR">Alarma de seguridad</label><br/>
<input name="as_COMODIDADES" id="CALAMB" type="checkbox" value="CALAMB" > <label for="CALAMB">Calefacción por ambiente</label><br/>
<input name="as_COMODIDADES" id="CANFUT" type="checkbox" value="CANFUT" > <label for="CANFUT">Cancha de fútbol</label><br/>
<input name="as_COMODIDADES" id="CANTENIS" type="checkbox" value="CANTENIS" > <label for="CANTENIS">Cancha de tenis</label><br/>
<input name="as_COMODIDADES" id="CONINT" type="checkbox" value="CONINT" > <label for="CONINT">Conexión a internet</label><br/>
<input name="as_COMODIDADES" id="HOGLEÑA" type="checkbox" value="HOGLEÑA" > <label for="HOGLEÑA">Hogar a leña</label><br/>
</div>
<div style="float:left;">
<input name="as_COMODIDADES" id="JARD" type="checkbox" value="JARD" > <label for="JARD">Jardín</label><br/>
<input name="as_COMODIDADES" id="PARR" type="checkbox" value="PARR" > <label for="PARR">Parrilla</label><br/>
<input name="as_COMODIDADES" id="PILNAT" type="checkbox" value="PILNAT" > <label for="PILNAT">Pileta</label><br/>
<input name="as_COMODIDADES" id="PORTELEC" type="checkbox" value="PORTELEC" > <label for="PORTELEC">Portón eléctrico</label><br/>
<input name="as_COMODIDADES" id="VIGI" type="checkbox" value="VIGI" > <label for="VIGI">Seguridad</label><br/>
<input name="as_COMODIDADES" id="TELEF" type="checkbox" value="TELEF" > <label for="TELEF">Teléfono</label><br/>
<input name="as_COMODIDADES" id="TVXCABLE" type="checkbox" value="TVXCABLE" > <label for="TVXCABLE">TV por cable</label><br/>
</div>
</div>
</div>
</form>
<br/>
<div style="clear:both"></div>
<div style="width:800px;  margin-right:40px;">
<input type="button" value="Siguiente paso!"
       style="cursor:pointer; background:#fec; float:right;" onclick="nuevo_inmueble()"/>
</div>
<div style="clear:both"></div>
