<html>
    <head>
        <title>Lo alquilamos, hoy!</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <script src="scripts/jquery-1.4.2.min.js"  type="text/javascript"></script>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '166681046739019',
                    status     : true, 
                    cookie     : true,
                    xfbml      : true,
                    oauth      : true
                });

                FB.Event.subscribe('auth.login', function(response) {
                    window.location.reload();
                });
            };
            (function(d){
                var js, id = 'facebook-jssdk';
                if (d.getElementById(id))
                {return;}
                js = d.createElement('script');
                js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                d.getElementsByTagName('head')[0].appendChild(js);
                /*getElementById('mostrar').innerHTML = d;*/
            }(document));
        </script>
    </head>
    <body>
        <div style="margin: 0 auto 0 auto; width: 100%;">
            <div onclick="fblogin();return false;" style="cursor: pointer;">FB Login permisos</div>
            <script>
                function fblogin() {
                    FB.login(function(response) {}, {perms:'email,user_checkins, email, read_friendlists, user_location, publish_stream'});
                }
            </script>
            <div onclick="fbapi();return false;" style="cursor: pointer;">FB Mostrar datos</div>
            <script>
                function fbapi(){
                    FB.api('/me', function(response) {
                        $("#mostrar").show();
                        var user_data = "<div>Logged user with FB_id: " +response.id + "</div>\n\
                        <div>Tu nombre es: <a href ="+response.link+">"+response.name+"</a></div>\n\
                        <div>Tu email es: " +response.email+ "</div>\n\
                        <img src='http://graph.facebook.com/"+response.id+"/picture'>";
                        document.getElementById('mostrar').innerHTML = user_data;         
                    });

                }
            </script>
            <div id="mostrar"></div>
        </div>
    </body>
</html>