<html>
    <head>
        <title>Lo alquilamos, hoy!</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <script src="scripts/main.js" type="text/javascript"></script>
        <script src="scripts/jquery-1.4.2.min.js"  type="text/javascript"></script>
        <!--<script src="scripts/facebook.js" type="text/javascript"></script>-->
        <!--<script src="http://connect.facebook.net/en_US/all.js"></script>-->
        <link href="css/estilos.css" rel="stylesheet" type="text/css" />

        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '166681046739019',
                    status     : true, 
                    cookie     : true,
                    xfbml      : true,
                    oauth      : true
                });

                FB.Event.subscribe('auth.login', function(response) {
                    window.location.reload();
                });
            };

            (function(d){
                var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                d.getElementsByTagName('head')[0].appendChild(js);
            }(document));
        </script>
    </head>
    <body onload="todo()">
        <div class="pagina" style="padding-left: 20px; padding-right: 20px;"> <!-- PAGINA -->
            
            <!--
            <div id="conectar_facebook">
                <div onclick="login();">Contectarse a Facebook</div>
            </div>
            <div onclick="publish();">Publicar algo en tu muro</div>
            -->

            <!-- FACEBOOK
            <div id="conectar_facebook"></div>
            <div id="fb-root"></div>
            <fb:login-button id="fb-auth">Login</fb:login-button>
            <div id="user-info"></div>
            <div id="debug"></div>
            END FACEBOOK -->

            <a href="#" onclick="fblogin();return false;">FACEBOOK</a>
            <script>
                function fblogin() {
                    FB.login(function(response) {}, {perms:'email,user_checkins, email, read_friendlists, user_location, publish_stream'});
                }
            </script>

            <div class='menuUP'>
                <div id="cookie"></div>
            </div>
            <div style="height: 40px;"></div>
            <div style="height: 50px;">
                <div class="logo">loalquilamos<span style="color:#cf3;">.com.ar</span></div>
                <div class="barra"></div>
            </div>

            <div id="center_div">
                <div style="padding: 10px;">


                    <!-- BUSCADOR -->
                    <span style="font-size:24px">Alquiler de Propiedades</span><br/>
                    <div id="buscador">

                        <div class="menu">
                            <div class="menu-nombre">Tipo de inmueble</div>
                            <div style="float:left;">
                                <select id="buscar_tipo_inmueble">
                                    <option value="1">Casas</option>
                                    <option value="2">Departamentos</option>
                                    <option value="3">PH</option>
                                    <option value="4">Countries y barrios cerrados</option>
                                    <option value="5">Quintas</option>
                                    <option value="6">Terrenos o lotes</option>
                                </select>
                            </div>
                        </div>
                        <div class="menu">
                            <div class="menu-nombre">Zona geografica:</div>
                            <div style="float:left;">
                                <select id="buscar_zona_geografica">	    		
                                    <option value="3642" >Capital Federal</option>         
                                    <option value="5055">Bs.As. G.B.A. Zona Norte</option>
                                    <option value="2277">Bs.As. G.B.A. Zona Oeste</option>
                                    <option value="3979">Bs.As. G.B.A. Zona Sur</option>		            
                                    <option value="4314">Buenos Aires Costa Atlántica</option>		            
                                    <option value="587" >Buenos Aires Interior</option>		            
                                    <option value="2811">Catamarca</option>		            
                                    <option value="3542">Chaco</option>		            
                                    <option value="5785">Chubut</option>		            
                                    <option value="1854">Córdoba</option>		            
                                    <option value="488" >Corrientes</option>		            
                                    <option value="4364">Entre Ríos</option>		            
                                    <option value="5474">Formosa</option>		            
                                    <option value="1571">Jujuy</option>		            
                                    <option value="2700">La Pampa</option>		            
                                    <option value="139" >La Rioja</option>		            
                                    <option value="5265">Mendoza</option>		            
                                    <option value="5169">Misiones</option>		            
                                    <option value="5970">Neuquén</option>		            
                                    <option value="3705">Río Negro</option>		            
                                    <option value="2360">Salta</option>		            
                                    <option value="295" >San Juan</option>		            
                                    <option value="3903">San Luis</option>		            
                                    <option value="6133">Santa Cruz</option>		            
                                    <option value="4659">Santa Fe</option>		            
                                    <option value="4096">Santiago Del Estero</option>		            
                                    <option value="2350">Tierra Del Fuego</option>		           
                                    <option value="2">Tucumán</option>		            
                                    <option value="5540">Zona Exterior</option>		            
                                </select>
                            </div>
                        </div>


                        <div class="menu">
                            <div class="menu-nombre">Dormitorios: </div>
                            <select id="buscar_dormitorios">
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">Más de 4</option>
                            </select>
                        </div>

                        <div class="menu">
                            <div class="menu-nombre">Ambientes: </div>
                            <select id="buscar_ambientes">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected>4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">9</option>
                                <option value="11">Más de 11</option>
                            </select>
                        </div>

                        <div style="float:right;">
                            <input type="button" value="Buscar!" style="cursor:pointer; background:#fec; float:right " onclick="buscar()" /></div>
                    </div> <!-- END BUSCADOR -->



                    <br/><br/><br/>
                    <!--RESULTADOS-->
                    <div class="resultados">
                        <!--
                        <?php
                        for ($i = 0; $i < 2; $i++) {
                            echo "<div class='double'>";
                            echo "<div style='float:left; width:200px;'>";
                            echo "<span class='lugar'>Mar de Las Pampas</span><br/>";
                            echo "<div class='precio'>Quincena <span class='pesos'>$3,500.00</span></div>";
                            echo "<div class='descripcion'>Cocina, Comedor, Ba&ntilde;o, 2 dormitorios, 1 Matrimonial, 2 individuales</div>";
                            echo "</div>";
                            echo "<div style='float:right; width:200px; height:120px; border:solid 1px #4faced'>";
                            echo "<img src='img/archive/3949_1.jpg' border='0'/>";
                            echo "</div></div>";
                        }
                        ?>
                        -->
                    </div>
                    <div style="clear:both"></div>
                    <div id="error_en_buscar"></div>
                </div>
            </div>

            <br/><div class="pie">

                <div class="texto-pie">Copyright 2011 - All rights reserved - www.loalquilamos.com.ar</div>

            </div>

        </div> <!-- END PAGINA -->
    </body>
</html>