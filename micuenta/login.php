<?php
header('Content-Type: text/html; charset=utf-8');

if (isset($_COOKIE['loalquilamos']))
    header("location: ../index.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Lo alquilamos, hoy!</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <script src="../scripts/main.js" type="text/javascript"></script>
        <script src="../scripts/jquery-1.4.2.min.js"  type="text/javascript"></script>
        <link href="../css/estilos.css" rel="stylesheet" type="text/css" />
        <meta name="google-site-verification" content="dsowz8aUodCkxWNrAkDCmTl43utO3B4bwgJJcPC1L8Y" />
    </head>
    <body>
        <center>
            <div class="ancho-login">
                <div class='menuUP_login'>
                    <div class='menuItem'><a href="../index.php">Home</a></div>
                </div><br/><br/>
                <div class="logo_login">loalquilamos<span style="color:#cf3;">.com.ar</span></div><br/>
                <div style="clear: both;"></div>
                <div class="center-div-login" id="center_div">
                    <div class="titulo-login">Ingresar a loalquilamos</div>
                    <hr size="1px" noshade="noshade" color="#4faced" /><br/>

                    <div class="contenedor-campos-login" style="padding-left: 25%;">
                        <div class="campo-login">
                            <span class="campo-nombre-login">E-mail:</span>
                            <input class="campo-input-login" id="email" type="text" size="30px" maxlength="30" />
                        </div><br/><br/>
                        <div class="campo-login">
                            <span class="campo-nombre-login">Contraseña:</span>
                            <input class="campo-input-login" id="pss" type="text" size="30px" maxlength="30" />
                        </div>
                    </div><br/><br/>
                    <input class="boton" type="submit" value="Entrar" name="login" onclick="login()"/>
                </div>
                <div id="login_error"></div>
                <div style="clear: both;"></div><br/>
                <div class="login-pie">¿ No tenes una cuenta en loalquilamos ? <a href="registro.php">Registrate</a></div>
            </div> <!-- ANCHO LOGIN -->
        </center>
    </body>
</html>