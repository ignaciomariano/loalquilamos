<html>
<head>
	<title>Lo alquilamos, hoy!</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link href="css/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <div style="padding: 50px;" > <!-- PAGINA -->

<!-- FACEBOOK --------------------------------------------------------------------------->
        <div id="fb-root"></div>
        <script type="text/javascript">
            var button;
            var userInfo;

            window.fbAsyncInit = function() {
                FB.init({
                    appId: '166681046739019',
                    status: true,
                    cookie: true,
                    xfbml: true,
                    oauth: true
                });

                function updateButton(response) {
                    button = document.getElementById('fb-auth');
                    userInfo = document.getElementById('user-info');

                    if (response.authResponse) {
                        //user is already logged in and connected
                        document.location.href="index.php";
                    } else {
                        //user is not connected to your app or logged out
                        //button.innerHTML = 'Login';
                        button.onclick = function() {
                            FB.login(function(response) {
                                if (response.authResponse) {
                                    FB.api('/me', function(info) {
                                    });
                                } else {
                                    //user cancelled login or did not grant authorization
                                }
                            }, {scope:'email,user_birthday,status_update,publish_stream,user_about_me'});
                                
                        }
                    }
                }

                // run once with current status and whenever the status changes
                FB.getLoginStatus(updateButton);
                FB.Event.subscribe('auth.statusChange', updateButton);
            };

            (function() {
                var e = document.createElement('script');
                e.async = true;
                e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
                document.getElementById('fb-root').appendChild(e);
            }());



        </script>

        <fb:login-button id="fb-auth">Login</fb:login-button>
        <div id="user-info"></div>
        <div id="debug"></div>
<!-- END FACEBOOK ----------------------------------------------------------------------->

    </div> <!-- END PAGINA -->
</body>
</html>