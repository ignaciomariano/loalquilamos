<html>
<head>
	<title>Lo alquilamos, hoy!</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link href="css/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <div class="pagina" style="padding-left: 20px; padding-right: 20px;"> <!-- PAGINA -->

<!-- FACEBOOK --------------------------------------------------------------------------->
        <div id="fb-root"></div>
        <script type="text/javascript">
            var button;
            var userInfo;

            window.fbAsyncInit = function() {
                FB.init({
                    appId: '166681046739019',
                    status: true,
                    cookie: true,
                    xfbml: true,
                    oauth: true
                });

                function updateButton(response) {
                    button       =   document.getElementById('fb-auth');
                    userInfo     =   document.getElementById('user-info');

                    if (response.authResponse) {
                        //user is already logged in and connected
                        FB.api('/me', function(info) {
                            login(response, info);
                        });
                    } else {
                        //user is not connected to your app or logged out
                        //button.innerHTML = 'Login';
                        button.onclick = function() {
                            FB.login(function(response) {
                                if (response.authResponse) {
                                    FB.api('/me', function(info) {
                                        login(response, info);
                                    });
                                } else {
                                    //user cancelled login or did not grant authorization
                                }
                            }, {scope:'email,user_birthday,status_update,publish_stream,user_about_me'});
                        }
                    }
                }

                // run once with current status and whenever the status changes
                FB.getLoginStatus(updateButton);
                FB.Event.subscribe('auth.statusChange', updateButton);
            };

            (function() {
                var e = document.createElement('script');
                e.async = true;
                e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
                document.getElementById('fb-root').appendChild(e);
            }());


            function login(response, info){
                if (response.authResponse) {
                    //var accessToken = response.authResponse.accessToken;
                    userInfo.innerHTML = '<img src="https://graph.facebook.com/' + info.id + '/picture">'
                                            + info.name
                                            //+ "<br /> Your Access Token: " + accessToken;
                    //button.innerHTML = 'Logout';
                    document.getElementById('other').style.display = "block";
                }
            }

            function logout(response){
                userInfo.innerHTML =   "";
                document.getElementById('debug').innerHTML =   "";
                document.getElementById('other').style.display =   "none";

            }
        </script>

        <fb:login-button id="fb-auth">Login</fb:login-button>
        <div id="user-info"></div>
        <div id="debug"></div>
<!-- END FACEBOOK ----------------------------------------------------------------------->

    </div> <!-- END PAGINA -->
</body>
</html>