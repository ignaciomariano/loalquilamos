
function movein(which,html) {
    which.style.background='#4DD734'
}
function moveout(which){
    which.style.background='#F8FAF5'
}
function get(id) {
    return document.getElementById(id);
}
function redirect(url,div,params)
{
    var p= "";
    if (get(div))
    { 
        var show =  get(div); 
        $.ajax(
        {
			
            type:"POST",
            url:url,
            data: p,
            success: function(result)
            {
                show.innerHTML = result;
            }
        }); 
    }
}
	
function nuevousuario(){
    var i = 0;
    var parametros = "";
    while (document.forms[0].elements[i]){
        parametros+="p"+i+"="+document.forms[0].elements[i].value+"&";
        i++;
    }
    parametros = parametros.slice(0, -1)
    alert(parametros);
    $.ajax(
    {
        type:"POST",
        url:"nuevo_usuario.php",
        data: parametros,
        success: function(result)
        {
            if(result == "0")
                get("registro_error").innerHTML = "El usuario ya existe";
            else
                location.href="/loalquilamos/index.php";
        }
    }
    )	
}

function comprobarNumero(str,diverror){
    var myRegxp = /^[0-9]+$/; //solo numeros
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(2,diverror);
    else
        mostrarError(0,diverror);
}
function mostrarError(codigoerror,diverror){
    var errores = new Array ("","Usa solo letras.","Usa solo numeros.","Usa el formato nombre@ejemplo.com.",
        "Ingresa al menos 2 caracteres.","Ingresa al menos 6 caracteres.",
        "Las claves son diferentes","El campo esta vacio");
    get(diverror).innerHTML = errores[codigoerror];
}
function comprobarTexto(str,diverror){
    var myRegxp = /^[a-zA-Z\s]+$/; //solo letras
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(1,diverror);
    else
        mostrarError(0,diverror);
}
function comprobarClave(diverror){
    var clave = (get("clave"))?get("clave").value:0;
    var repiteclave = (get("repiteclave"))?get("repiteclave").value:0;
    if( clave != repiteclave)
        mostrarError(6,diverror);
    else
        mostrarError(0,diverror);
}
function comprobarEmail(str,diverror){
    var myRegxp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; //comprobar Email
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(3,diverror);
    else
        mostrarError(0,diverror);
}
function nuevo_inmueble() 
{
    var parametros = "";
    var i = 0;
    while (document.forms[0].elements[i]){
        parametros+="p"+i+"="+document.forms[0].elements[i].value+"&";
        i++;
    }
    parametros = parametros.slice(0, -1)
    alert(parametros);
    $.ajax
    (
    {
        type:"POST",
        url:"nuevo_inmueble.php",
        data: parametros, 
        success: function(result)

        { 
            if(result==0)
                get("registro_error").innerHTML = "La publicacion ya existe ya existe";
            else
            {
                alert("El inmueble ha sido publicado!");
                redirect('publicar_paso2.php','mostrar_panel',0)					
            }
        }
    }
    )	
}

function buscar()
{
    var zona_geo = get("buscar_zona_geografica");
    var tipo_inm = get("buscar_tipo_inmueble");

    var parametros ="tipo_inm="+tipo_inm+"&zona_geo="+zona_geo;
    $.ajax(
    {
        type:"POST",
        url:"buscar.php",
        data: parametros,
        success: function(result)
        { 
            if(result)
                get("error_en_buscar").innerHTML = "Estos son los resultados";
            else
                get("error_en_buscar").innerHTML = "No hay resultados para la busqueda realizada";
        }
    }
    )
}

function actulizar_mis_datos()
{
    var res = new Array();
    var e = document.getElementsByClassName("menu-campo");
    var i = 0;
    var parametros = "";
    while (e[i])
    {
        if (e[i].value.length==0)
        {
            e[i].style.background = '#ff3300';
            err = 1;
        }
		
        parametros += "&t"+i+"="+e[i].value;
        i++;
    }
    parametros+="&cant="+(i-1);
    $.ajax
    (
    {
        type:"POST",
        url:"actualizar_mis_datos.php",
        data: parametros,
        success: function(result)
        { 
            if(result==1)
                get("resultado_actualizar_mis_datos").innerHTML = "Los cambios se han realizado con exito!";
            else
            {
                get("resultado_actualizar_mis_datos").innerHTML = "No se pudo actualizar!";			
            }
        }
    }
    )


}

function logout(){
    if(getCookie('loalquilamos')){
        var cook = get("cook");
        document.cookie = "loalquilamos=" + cook.value + ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
        document.location.href = "index.php";
    }
}
    
function getCookie(b){
    var c,a,e,d=document.cookie.split(";");
    for(c=0;c<d.length;c++){
        a=d[c].substr(0,d[c].indexOf("="));
        e=d[c].substr(d[c].indexOf("=")+1);
        a=a.replace(/^\s+|\s+$/g,"");
        if(a==b){
            return unescape(e)
        }
    }
    return 0;
}

function todo(){
    checkLogin();
}

function checkLogin(){
    var cookie = getCookie("loalquilamos");
    if(cookie){
        get("cookie").innerHTML  =  "<div class='menuItem' id='cook'>Hola "+ cookie +"</div>\n\
                                    <div class='menuItem' onclick='logout()'> Salir</div>\n\
                                    <div class='menuItem'><a href='micuenta/index.php'>Mi cuenta</a></div>\n\
                                    <div class='menuItem'><a href='publicar/index.php'>Publicar</a></div>";
    }else{
        get("cookie").innerHTML = "<div class='menuItem'><a href='micuenta/registro.php'>Reg&iacute;strate</a></div>\n\
                                   <div class='menuItem'><a href='micuenta/login.php'>Ingresar</a></div>\n\
                                   <div class='menuItem'><a href='#'>Mi Cuenta</a></div>\n\
                                   <div class='menuItem'><a href='#'>Publicar</div></a>";
    }
}


function login(){
    var result = 0;
    var parametros = "";
    var email = get("email");
    var password = get("pss");  
    if(email.value.length == 0 || password.value.length == 0){
        alert("complete los campos");
        //get("result-login").innerHTML = "Complete los dos campos";
        return 1;
    }
    parametros = "p0="+ email.value + "&p1=" + password.value;
    $.ajax({
        type:"POST",
        url:"php/login.php",
        data: parametros,
        success: function(result){
            if(result){
                alert(parametros);
                var hoy = new Date();
                var msEnXDias = 31 * 24 * 60 * 60 * 1000;  
                hoy.setTime(hoy.getTime() + msEnXDias);
                var dias = hoy.toGMTString();
                document.cookie = "loalquilamos=" + email.value + "; expires=" + dias;
                document.location.href = "index.php";
            }else{
                alert("Pone bien el usuario y el password");
            }
            alert(result);
        }
    })
    return 0;
}   